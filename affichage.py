from tkinter import*
import tkinter.font as tkFont
from week2.grid_demineur import*

# coding: utf8

root = Tk()
root.title('Démineur')

Zero = "Zero"
One = "One"
Two = "Two"
Three = "Three"
Four = "Four"
Five = "Five"
Six = "Six"
Seven = "Seven"
Eight = "Eight"
Empty = "Empty"
Flag = "Flag"
Bomb = "Bomb"

NUMBER_TO_IMAGE = {-1: Bomb, 0: Zero, 1: One, 2: Two, 3: Three, 4: Four, 5: Five, 6: Six, 7: Seven, 8: Eight}

THEMES = {"classic": {Zero: PhotoImage(file='styles/classic/n0.gif'), One: PhotoImage(file='styles/classic/n1.gif'), Two: PhotoImage(file='styles/classic/n2.gif'), Three: PhotoImage(file='styles/classic/n3.gif'), Four: PhotoImage(file='styles/classic/n4.gif'), Five: PhotoImage(file='styles/classic/n5.gif'), Six: PhotoImage(file='styles/classic/n6.gif'), Seven: PhotoImage(file='styles/classic/n7.gif'), Eight: PhotoImage(file='styles/classic/n8.gif'), Empty: PhotoImage(file='styles/classic/empty.gif'), Bomb: PhotoImage(file='styles/classic/bomb.gif'), Flag: PhotoImage(file='styles/classic/flag.gif')}, "minimalist": {Zero: PhotoImage(file='styles/classic/n0.gif'), One: PhotoImage(file='styles/minimalist/n1.gif'), Two: PhotoImage(file='styles/minimalist/n2.gif'), Three: PhotoImage(file='styles/minimalist/n3.gif'), Four: PhotoImage(file='styles/minimalist/n4.gif'), Five: PhotoImage(file='styles/minimalist/n5.gif'), Six: PhotoImage(file='styles/minimalist/n6.gif'), Seven: PhotoImage(file='styles/minimalist/n7.gif'), Eight: PhotoImage(file='styles/minimalist/n8.gif'), Empty: PhotoImage(file='styles/minimalist/empty.gif'), Bomb: PhotoImage(file='styles/minimalist/bomb.gif'), Flag: PhotoImage(file='styles/minimalist/flag.gif')}, "cartoon": {Zero:PhotoImage(file='styles/cartoon/0.png'), One:PhotoImage(file='styles/cartoon/1.png'), Two:PhotoImage(file='styles/cartoon/2.png'), Three:PhotoImage(file='styles/cartoon/3.png'), Four:PhotoImage(file='styles/cartoon/4.png'), Five:PhotoImage(file='styles/cartoon/5.png'), Six:PhotoImage(file='styles/cartoon/6.png'), Seven:PhotoImage(file='styles/cartoon/7.png'), Eight:PhotoImage(file='styles/cartoon/8.png'), Empty:PhotoImage(file='styles/cartoon/empty.png'), Flag:PhotoImage(file='styles/cartoon/flag.png'), Bomb:PhotoImage(file='styles/cartoon/bomb.png')}, "cs":{Zero:PhotoImage(file='styles/cs/0.png'), One:PhotoImage(file='styles/cs/1.png'), Two:PhotoImage(file='styles/cs/2.png'), Three:PhotoImage(file='styles/cs/3.png'), Four:PhotoImage(file='styles/cs/4.png'), Five:PhotoImage(file='styles/cs/5.png'), Six:PhotoImage(file='styles/cs/6.png'), Seven:PhotoImage(file='styles/cartoon/7.png'), Eight:PhotoImage(file='styles/cs/8.png'), Empty:PhotoImage(file='styles/cs/empty.png'), Flag:PhotoImage(file='styles/cs/flag.png'), Bomb:PhotoImage(file='styles/cs/bomb.png')}, "csupelec":{Zero:PhotoImage(file='styles/cs/0.png'), One:PhotoImage(file='styles/cs/1.png'), Two:PhotoImage(file='styles/cs/2.png'), Three:PhotoImage(file='styles/cs/3.png'), Four:PhotoImage(file='styles/cs/4.png'), Five:PhotoImage(file='styles/cs/5.png'), Six:PhotoImage(file='styles/cs/6.png'), Seven:PhotoImage(file='styles/cartoon/7.png'), Eight:PhotoImage(file='styles/cs/8.png'), Empty:PhotoImage(file='styles/cs/empty.png'), Flag:PhotoImage(file='styles/cs/flag.png'), Bomb:PhotoImage(file='styles/csupelec/bomb.png')}, "moche": {Zero:PhotoImage(file='styles/moche/0.png'), One:PhotoImage(file='styles/moche/1.png'), Two:PhotoImage(file='styles/moche/2.png'), Three:PhotoImage(file='styles/moche/3.png'), Four:PhotoImage(file='styles/moche/4.png'), Five:PhotoImage(file='styles/moche/5.png'), Six:PhotoImage(file='styles/moche/6.png'), Seven:PhotoImage(file='styles/moche/7.png'), Eight:PhotoImage(file='styles/moche/8.png'), Empty:PhotoImage(file='styles/moche/empty.png'), Flag:PhotoImage(file='styles/moche/flag.png'), Bomb:PhotoImage(file='styles/moche/bomb.png')}, "funky":{Zero:PhotoImage(file='styles/funky/0.png'), One:PhotoImage(file='styles/funky/1.png'), Two:PhotoImage(file='styles/funky/2.png'), Three:PhotoImage(file='styles/funky/3.png'), Four:PhotoImage(file='styles/funky/4.png'), Five:PhotoImage(file='styles/funky/5.png'), Six:PhotoImage(file='styles/funky/6.png'), Seven:PhotoImage(file='styles/funky/7.png'), Eight:PhotoImage(file='styles/funky/8.png'), Empty:PhotoImage(file='styles/funky/empty.png'), Flag:PhotoImage(file='styles/funky/flag.png'), Bomb:PhotoImage(file='styles/funky/bomb.png')}, "funkiest": {Zero: PhotoImage(file='styles/funkiest/0.png'), One:PhotoImage(file='styles/funkiest/1.png'), Two:PhotoImage(file='styles/funkiest/2.png'), Three:PhotoImage(file='styles/funkiest/3.png'), Four:PhotoImage(file='styles/funkiest/4.png'), Five:PhotoImage(file='styles/funkiest/5.png'), Six:PhotoImage(file='styles/funkiest/6.png'), Seven:PhotoImage(file='styles/cartoon/7.png'), Eight:PhotoImage(file='styles/cartoon/8.png'), Empty:PhotoImage(file='styles/cartoon/empty.png'), Flag:PhotoImage(file='styles/funkiest/flag.png'), Bomb:PhotoImage(file='styles/funkiest/bomb.png')}}

# Ce dictionnaire permettra de passer de la valeur dans la grille à l'image associée

size = 15
nb_bombs = 40
grid = init_grid(size, nb_bombs)
grid_height = len(grid)
grid_width = len(grid)
flag_grid = [[[False, False] for i in range(grid_width)] for j in range(grid_height)]
is_game_over = False
premier_coup = True
theme="csupelec"
# flag_grid est un tableau de couples de booléens ou le premier booléen vaut True si on a déjà cliqué sur la case, False sinon et le 2e vaut True si un drapeau est sur la case


def buttons_init(game_grid):
    '''
    Initialise le tableau de boutons pour tkinter
    :param game_grid: grille de jeu
    :return: tableau des boutons
    '''
    height = len(game_grid)
    width = len(game_grid[0])
    boutons = [[[] for y in range(width)] for x in range(height)]
    for i in range(height):
        for j in range(width):
            bouton = Button()
            bouton.grid(row=i,column=j)
            bouton['image'] = THEMES[theme][Empty]
            boutons[i][j] = bouton
    return boutons


boutons = buttons_init(grid)


def pressed(event, click):
    '''
    Gere l'impact des clics sur la grille, la variable click permettra de différencier le clic droit du clic gauche
    :param event:
    :param click: "left" si clic gauche, "right" si clic droit
    '''
    global premier_coup  #La variable globale premier_coup permet de différencier le tout premier clic des autres (obligatoirement  sur une case sans bombes autour)
    global grid
    button = event.widget
    if not is_game_over:
        j = -1
        for i in range(grid_height):  # permet de récupérer les indices i,j du bouton sur lequel le joueur à cliqué
            if button in boutons[i]:
                j = list(boutons[i]).index(button)
                break
        if premier_coup:
            while grid[i][j] != 0:
                grid=init_grid(size, nb_bombs)
            premier_coup = False
        if click == "right":  # si c'est un clic droit, on ajoute/enlève un drapeau
            flag_grid[i][j][1] = not flag_grid[i][j][1]
        if click == "left":
            if not flag_grid[i][j][0]:  # si le joueur n'a pas encore cliqué sur la case
                if grid[i][j] == -1:  # si c'est une bombe, c'est perdu
                    game_over(grid)
                else:
                    clicked_list = cases_a_cliquer(grid, i, j, flag_grid)  # dans le cas d'une case sans bombes autour, les cases voisines sont elles aussi révélées
                    for position in clicked_list:
                        x, y = position[0], position[1]
                        flag_grid[x][y][0] = True
        display_grid(grid, flag_grid, boutons)
        not_clicked=[]  #liste des cases sans bombes qui n'ont pas encore été cliquées
        for i in range(grid_height):
            for j in range(grid_width):
                if not flag_grid[i][j][0]:
                    if grid[i][j] != -1:
                        not_clicked.append((i,j))
        if not_clicked == []:
            you_win()

def you_win():
    '''
    déclenche la fin de partie (victoire)
    :return:
    '''
    global is_game_over
    win=Toplevel(root)
    Label(win, text="You win ☻", font=tkFont.Font(family="Verdana", size=20, weight="bold"), fg="#027C00", relief=SUNKEN).pack()
    is_game_over = True



root.bind("<Button-1>", lambda event: pressed(event, "left"))  # clic gauche
root.bind("<Button-2>", lambda event: pressed(event, "right"))  # clic droit sur Mac
root.bind("<Button-3>", lambda event: pressed(event, "right"))  # clic droit


def display_grid(game_grid, flag_grid, boutons):
    '''
    affiche la grille de jeu (les boutons)
    :param game_grid: grille de jeu contenant pour chaque case le nombre de bombes autour (-1 si une bombe est dans la case)
    :param flag_grid: grille de (Bool,Bool) où le premier Bool vaut True ssi la case a été cliquée et le second vaut True ssi un drapeau est dans la case
    :param boutons: liste des boutons
    '''
    height = len(game_grid)
    width = len(game_grid[0])
    for i in range(height):
        for j in range(width):
            bouton = boutons[i][j]
            if flag_grid[i][j][0]:
                bouton['image'] = THEMES[theme][NUMBER_TO_IMAGE[game_grid[i][j]]]
            elif flag_grid[i][j][1]:
                bouton['image'] = THEMES[theme][Flag]
            else:
                bouton['image'] = THEMES[theme][Empty]


def get_bombs_position(game_grid):
    '''
    utile pour la fin de partie en cas de défaite
    :param game_grid: grille de jeu
    :return: liste des positions (i,j) des bombes
    '''
    height = len(game_grid)
    width = len(game_grid[0])
    bombs_list = []
    for i in range(height):
        for j in range(width):
            if game_grid[i][j] == -1:
                bombs_list.append((i, j))
    return bombs_list


def game_over(game_grid):
    '''
    déclenche la fin de partie (défaite)
    révèle toutes les bombes
    :param game_grid: grille de jeu
    '''
    global is_game_over
    bombs_list = get_bombs_position(game_grid)
    for position in bombs_list:
        x, y = position[0], position[1]
        flag_grid[x][y][0] = True
    display_grid(game_grid, flag_grid, boutons)
    lost = Toplevel(root)
    Label(lost, text="You lose ¯\_(ツ)_/¯", font=tkFont.Font(family="Verdana", size=20, weight="bold"), fg="#7C0000",relief=SUNKEN).pack()
    is_game_over = True




root.mainloop()
