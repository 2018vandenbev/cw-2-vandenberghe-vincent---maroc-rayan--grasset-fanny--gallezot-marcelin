from tkinter import *
import tkinter.font as tkFont
from random import *
from week2.grid_demineur import *

###Création fenetre menu et ses différentes options
fenetre=Tk()
fenetre.title('menu demineur')
value = StringVar()
value.set("")
value2 = StringVar()
value2.set("")
label_taille=Label(fenetre,text="Taille: (>4)")
label_taille.grid(row=5,column=0)
label_bombe=Label(fenetre,text="Nombre de bombes: (<50)")
label_bombe.grid(row=6,column=0)
label_espace=Label(fenetre)
label_espace.grid(row=3)
entree_taille = Entry(fenetre, textvariable=value, width=15)
entree_bombe = Entry(fenetre, textvariable=value2,width=15)
entree_taille.grid(row=5,column=1)
entree_bombe.grid(row=6,column=1)
label_theme=Label(fenetre, text="Choisissez un thème:",bg="salmon",width=45)
label_theme.grid(row=7,column=0,columnspan=2)
choice_th=Variable(fenetre, ('classic', 'minimalist', 'cartoon', 'cs','csupelec', 'moche','funky','funkiest'))
choice_theme=Listbox(fenetre,listvariable=choice_th,width=25)
choice_theme.grid(row=8,column=0,columnspan=2)
listbox_choice_theme=Listbox(fenetre)

choices = Variable(fenetre, ('Easy (grille 9x9, 10 bombes)', 'Medium (grille16x16, 40 bombes)', 'Hard (grille 22x22, 99 bombes)'))
label_level=Label(fenetre,text="Choissisez un niveau:",font=('Helvetica', '14'),bg="salmon",width=45)
label_level.grid(row=1,column=0,columnspan=2)
label_personnalisé=Label(fenetre,text="OU Partie personnalisée:",bg="salmon",width=45)
label_personnalisé.grid(row=4,column=0,columnspan=2)
listbox_choice_level=Listbox(fenetre,listvariable=choices,exportselection=0,height=3,width=25)
listbox_choice_level.grid(row=2,column=0,columnspan=2)

###Initialisation des variables pour pouvoir les utiliser de manière globale dans les programmes qui suivent
number=0
grid_size=0
grid=0
flag_grid=0
is_game_over = False
boutons=0
lost=0
win=0
premier_coup=True

###Raccourci pour le dictionnaire de Themes
Zero = "Zero"
One = "One"
Two = "Two"
Three = "Three"
Four = "Four"
Five = "Five"
Six = "Six"
Seven = "Seven"
Eight = "Eight"
Flag = "Flag"
Empty = "Empty"
Bomb = "Bomb"

THEMES=[{"Zero": PhotoImage(file='styles/classic/n0.gif'), "One": PhotoImage(file='styles/classic/n1.gif'), "Two": PhotoImage(file='styles/classic/n2.gif'), "Three": PhotoImage(file='styles/classic/n3.gif'), "Four": PhotoImage(file='styles/classic/n4.gif'), "Five": PhotoImage(file='styles/classic/n5.gif'), "Six": PhotoImage(file='styles/classic/n6.gif'), "Seven": PhotoImage(file='styles/classic/n7.gif'), "Eight": PhotoImage(file='styles/classic/n8.gif'), "Empty": PhotoImage(file='styles/classic/empty.gif'), "Bomb": PhotoImage(file='styles/classic/bomb.gif'), "Flag": PhotoImage(file='styles/classic/flag.gif')}, {"Zero": PhotoImage(file='styles/classic/n0.gif'), "One": PhotoImage(file='styles/minimalist/n1.gif'), "Two": PhotoImage(file='styles/minimalist/n2.gif'), "Three": PhotoImage(file='styles/minimalist/n3.gif'), "Four": PhotoImage(file='styles/minimalist/n4.gif'), "Five": PhotoImage(file='styles/minimalist/n5.gif'), "Six": PhotoImage(file='styles/minimalist/n6.gif'), "Seven": PhotoImage(file='styles/minimalist/n7.gif'), "Eight": PhotoImage(file='styles/minimalist/n8.gif'), "Empty": PhotoImage(file='styles/minimalist/empty.gif'), "Bomb": PhotoImage(file='styles/minimalist/bomb.gif'), "Flag": PhotoImage(file='styles/minimalist/flag.gif')}, {Zero:PhotoImage(file='styles/cartoon/0.png'), One:PhotoImage(file='styles/cartoon/1.png'), Two:PhotoImage(file='styles/cartoon/2.png'), Three:PhotoImage(file='styles/cartoon/3.png'), Four:PhotoImage(file='styles/cartoon/4.png'), Five:PhotoImage(file='styles/cartoon/5.png'), Six:PhotoImage(file='styles/cartoon/6.png'), Seven:PhotoImage(file='styles/cartoon/7.png'), Eight:PhotoImage(file='styles/cartoon/8.png'), Empty:PhotoImage(file='styles/cartoon/empty.png'), Flag:PhotoImage(file='styles/cartoon/flag.png'), Bomb:PhotoImage(file='styles/cartoon/bomb.png')}, {Zero:PhotoImage(file='styles/cs/0.png'), One:PhotoImage(file='styles/cs/1.png'), Two:PhotoImage(file='styles/cs/2.png'), Three:PhotoImage(file='styles/cs/3.png'), Four:PhotoImage(file='styles/cs/4.png'), Five:PhotoImage(file='styles/cs/5.png'), Six:PhotoImage(file='styles/cs/6.png'), Seven:PhotoImage(file='styles/cartoon/7.png'), Eight:PhotoImage(file='styles/cs/8.png'), Empty:PhotoImage(file='styles/cs/empty.png'), Flag:PhotoImage(file='styles/cs/flag.png'), Bomb:PhotoImage(file='styles/cs/bomb.png')}, {Zero:PhotoImage(file='styles/cs/0.png'), One:PhotoImage(file='styles/cs/1.png'), Two:PhotoImage(file='styles/cs/2.png'), Three:PhotoImage(file='styles/cs/3.png'), Four:PhotoImage(file='styles/cs/4.png'), Five:PhotoImage(file='styles/cs/5.png'), Six:PhotoImage(file='styles/cs/6.png'), Seven:PhotoImage(file='styles/cartoon/7.png'), Eight:PhotoImage(file='styles/cs/8.png'), Empty:PhotoImage(file='styles/cs/empty.png'), Flag:PhotoImage(file='styles/cs/flag.png'), Bomb:PhotoImage(file='styles/csupelec/bomb.png')},  {Zero:PhotoImage(file='styles/moche/0.png'), One:PhotoImage(file='styles/moche/1.png'), Two:PhotoImage(file='styles/moche/2.png'), Three:PhotoImage(file='styles/moche/3.png'), Four:PhotoImage(file='styles/moche/4.png'), Five:PhotoImage(file='styles/moche/5.png'), Six:PhotoImage(file='styles/moche/6.png'), Seven:PhotoImage(file='styles/moche/7.png'), Eight:PhotoImage(file='styles/moche/8.png'), Empty:PhotoImage(file='styles/moche/empty.png'), Flag:PhotoImage(file='styles/moche/flag.png'), Bomb:PhotoImage(file='styles/moche/bomb.png')},{Zero:PhotoImage(file='styles/funky/0.png'), One:PhotoImage(file='styles/funky/1.png'), Two:PhotoImage(file='styles/funky/2.png'), Three:PhotoImage(file='styles/funky/3.png'), Four:PhotoImage(file='styles/funky/4.png'), Five:PhotoImage(file='styles/funky/5.png'), Six:PhotoImage(file='styles/funky/6.png'), Seven:PhotoImage(file='styles/funky/7.png'), Eight:PhotoImage(file='styles/funky/8.png'), Empty:PhotoImage(file='styles/funky/empty.png'), Flag:PhotoImage(file='styles/funky/flag.png'), Bomb:PhotoImage(file='styles/funky/bomb.png')}, {Zero: PhotoImage(file='styles/funkiest/0.png'), One:PhotoImage(file='styles/funkiest/1.png'), Two:PhotoImage(file='styles/funkiest/2.png'), Three:PhotoImage(file='styles/funkiest/3.png'), Four:PhotoImage(file='styles/funkiest/4.png'), Five:PhotoImage(file='styles/funkiest/5.png'), Six:PhotoImage(file='styles/funkiest/6.png'), Seven:PhotoImage(file='styles/cartoon/7.png'), Eight:PhotoImage(file='styles/cartoon/8.png'), Empty:PhotoImage(file='styles/cartoon/empty.png'), Flag:PhotoImage(file='styles/funkiest/flag.png'), Bomb:PhotoImage(file='styles/funkiest/bomb.png')}]

def play_game():
    global win
    global THEMES
    global premier_coup
    global lost
    global is_game_over
    global grid
    global number
    global grid_size
    global listbox_choice_level
    global flag_grid
    global boutons
    global choice_theme
    entree_size=entree_taille.get()
    entree_bomb=entree_bombe.get()
    try :
        numbertheme=choice_theme.curselection()[0]
    except IndexError:
        label_error=Label(fenetre,text="Choisissez un theme svp",fg="red")
        label_error.grid(row=10,column=0,columnspan=2)
    try:
        int(entree_size)
        int(entree_bomb)
        int(entree_size)>0
        int(entree_bomb)>0
        number=int(entree_bomb)
        grid_size=int(entree_size)
    except ValueError:
        try:
            listbox_choice_level.curselection()[0]
        except IndexError:
            label_error_size=Label(fenetre,text="Choisissez une taille svp",fg="red")
            label_error_size.grid(row=11,column=0,columnspan=2)
        level=listbox_choice_level.curselection()[0]
        if level==0:
            number=10
            grid_size=9
        elif level==1:
            number=40
            grid_size=16
        else:
            number=99
            grid_size=22


    root = Toplevel(fenetre)
    root.title('Démineur')

    NUMBER_TO_IMAGE = {"-1": "Bomb", "0": "Zero", "1": "One", "2": "Two", "3": "Three", "4": "Four", "5": "Five", "6": "Six", "7": "Seven", "8": "Eight"}
    # Ce dictionnaire permettra de passer de la valeur dans la grille à l'image associée

    grid = init_grid(grid_size,number)
    grid_height = len(grid)
    grid_width = len(grid)
    flag_grid = [[[False, False] for i in range(grid_width)] for j in range(grid_height)]
    # flag_grid est un tableau de couples de booléens ou le premier booléen vaut True si on a déjà cliqué sur la case, False sinon
    if flag_grid==[[[False, False] for i in range(grid_width)] for j in range(grid_height)]:
        premier_coup=True                               ###Permet d'éviter que lorsque l'on ferme manuellement la fenetre, premier_coup=false


    def buttons_init(game_grid): # Initialise le tableau de boutons pour tkinter
        height = len(game_grid)
        width = len(game_grid[0])
        boutons = [[[] for y in range(width)] for x in range(height)]
        for i in range(height):
            for j in range(width):
                bouton = Button(root)
                bouton.grid(row=i,column=j)
                bouton['image'] = THEMES[numbertheme]["Empty"]
                boutons[i][j] = bouton
        return boutons


    boutons = buttons_init(grid)


    def pressed(event, click):  # Gere l'impact des clics sur la grille, la variable click permettra de différencier le clic droit du clic gauche
        global premier_coup
        global grid
        global grid_size
        global number
        button = event.widget
        if not is_game_over:
            j = -1
            for i in range(grid_height):  # permet de récupérer les indices i,j du bouton sur lequel le joueur à cliqué
                if button in boutons[i]:
                    j = list(boutons[i]).index(button)
                    break
            if premier_coup:
                while grid[i][j] != 0:
                    grid=init_grid(grid_size, number)
                premier_coup = False
            if click == "right":  # si c'est un clic droit, on ajoute/enlève un drapeau
                flag_grid[i][j][1] = not flag_grid[i][j][1]
            if click == "left":
                if not flag_grid[i][j][0]:  # si le joueur n'a pas encore cliqué sur la case
                    if grid[i][j] == -1:  # si c'est une bombe, c'est perdu
                        game_over(grid)
                    else:
                        clicked_list = cases_a_cliquer(grid, i, j, flag_grid)  # dans le cas d'une case sans bombes autour, les cases voisin
                        for position in clicked_list:
                            x, y = position[0], position[1]
                            flag_grid[x][y][0] = True
            display_grid(grid, flag_grid, boutons)
            not_clicked=[]
            for i in range(grid_height):
                for j in range(grid_width):
                    if not flag_grid[i][j][0]:
                        if grid[i][j] != -1:
                            not_clicked.append((i,j))
            if not_clicked == []:
                you_win()






    root.bind("<Button-1>", lambda event: pressed(event, "left"))
    root.bind("<Button-2>", lambda event: pressed(event, "right"))
    root.bind("<Button-3>", lambda event: pressed(event, "right"))



    def display_grid(game_grid, flag_grid, boutons):
        height = len(game_grid)
        width = len(game_grid[0])
        for i in range(height):
            for j in range(width):
                bouton = boutons[i][j]
                if flag_grid[i][j][0]:
                    bouton['image'] = THEMES[numbertheme][NUMBER_TO_IMAGE["{}".format(game_grid[i][j])]]
                elif flag_grid[i][j][1]:
                    bouton['image'] = THEMES[numbertheme]["Flag"]
                else:
                    bouton['image'] = THEMES[numbertheme]["Empty"]


    def get_bombs_position(game_grid): #renvoie la liste des positions (i,j) des bombes de la grille (utile pour la fin de partie)
        height = len(game_grid)
        width = len(game_grid[0])
        bombs_list = []
        for i in range(height):
            for j in range(width):
                if game_grid[i][j] == -1:
                    bombs_list.append((i, j))
        return bombs_list

    def restart():
            '''

            :return: ne return rien mais modifie la grille en la réitinialisant, de même que le tableau de drapeau
            '''
            global premier_coup
            global win
            global lost
            global grid
            global boutons
            global flag_grid
            global is_game_over
            is_game_over=False
            flag_grid = [[[False, False] for i in range(grid_width)] for j in range(grid_height)]
            grid=init_grid(grid_size,number)
            display_grid(grid,flag_grid,boutons)
            premier_coup=True
            if lost!=0:
                lost.destroy()
            if win!=0:
                win.destroy()

    def you_win():
        global is_game_over
        global win
        win=Toplevel(root)
        label_win=Label(win, text="You win ☻", font=tkFont.Font(family="Verdana", size=50, weight="bold"), fg="green")
        label_win.grid(row=0)
        is_game_over = True
        rest_butt=Button(win, text="Restart",command=restart,width=15)
        rest_butt.grid(row=1)




    def game_over(game_grid): #révèle toutes les bombes
        global lost
        global is_game_over
        bombs_list = get_bombs_position(game_grid)
        for position in bombs_list:
            x, y = position[0], position[1]
            flag_grid[x][y][0] = True
        display_grid(game_grid, flag_grid, boutons)
        lost = Toplevel(root)
        label=Label(lost, text="You lose ¯\_(ツ)_/¯", font=tkFont.Font(family="Verdana", size=50, weight="bold"), fg="red")
        label.grid(row=0)
        rest_but=Button(lost, text="Restart",command=restart,width=15)
        rest_but.grid(row=2)

        is_game_over = True


    restart_button=Button(root, text="Restart",command=restart,width=8)
    restart_button.grid(row=grid_size+1,column=0,columnspan=grid_size)
    quit_butt=Button(root,text="Quit",command=quit,width=8)
    quit_butt.grid(row=grid_size+2,column=0,columnspan=grid_size)
    root.mainloop()
quit_button=Button(fenetre, text="Quit",command=quit, width=15)
quit_button.grid(row=9,column=0)
play_button=Button(fenetre, text="Play",command=play_game,width=15 )
play_button.grid(row=9,column=1)
fenetre.mainloop()
