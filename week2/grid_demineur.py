from random import *


def create_grid(size):
    """
    crée une grille remplie de 0
    :param size: taille de la grille
    :return: une matrice carrée de taille size remplie de 0
    """
    game_grid = []   # on crée une grille initialement vide.
    for i in range(0, size):   # on fait une boucle qui parcourt chaque ligne de la grille
        game_grid.append(size*[0])   # a chaque ligne on rajoute une liste de 0 de taille size
    return game_grid   # on retourne la grille remplie de 0


def get_empty_tiles_positions(grid):
    """
    renvoie les positions (i,j) des cases vides dans la grille
    :param grid: la grille de jeu
    :return: une liste de couples (i,j), positions des 0 dans la grille
    """
    size = len(grid)   # on crée une variable contenant la taille de la grille
    empty_tiles_positions = []   # on crée une liste vide qui contiendra les positions des tuiles vides
    for i in range(size):   # on fait une double boucle pour parcourir les elements de la grille
        for j in range(size):
            if grid[i][j] == 0:   # si une tuile de la grille est vide
                empty_tiles_positions += [(i, j)]   # alors on enregistre les coordonnees dans la liste créee precedemment
    return empty_tiles_positions   # on retourne toute la liste contenant les positions des tuiles vides


def get_new_position(grid):
    """
    renvoie une position (i,j) aléatoire parmi les cases vides
    :param grid: la grille de jeu
    :return: un couple (i,j)
    """
    empty_tiles_positions = get_empty_tiles_positions(grid)   # on crée la liste des positions libres
    return empty_tiles_positions[randrange(len(empty_tiles_positions))]   # choisit une position aléatoire parmi la liste de cases vides


def get_number_bombs(grid,i,j):
    """
    renvoie le nombre de bombes autour de la case (i,j), sert à créer une grille de jeu fonctionnelle
    :param grid: la grille de jeu
    :param i: la case est dans la i-ème ligne de la grille
    :param j: la case est dans la j-ème colonne de la grille
    :return: un nombre entre 0 et 8, nombre de bombes autour de la case (i,j)
    """
    size = len(grid)
    n = 0   # on initialise le nombre de bombes à 0
    if i > 0:   # chaque if correspond à une case voisine (8 if)
        if grid[i-1][j] == -1:
            n += 1   # on incrémente 1 s'il y a une bombe dans la case voisine en question
    if j > 0:
        if grid[i][j-1] == -1:
            n += 1
    if i < size - 1:
        if grid[i+1][j] == -1:
            n += 1
    if j < size - 1:
        if grid[i][j+1] == -1:
            n += 1
    if i > 0 and j < size - 1:
        if grid[i-1][j+1] == -1:
            n += 1
    if i < size - 1 and j < size - 1:
        if grid[i+1][j+1] == -1:
            n += 1
    if i > 0 and j > 0:
        if grid[i-1][j-1] == -1:
            n += 1
    if i < size - 1 and j > 0:
        if grid[i+1][j-1] == -1:
            n += 1
    return n


def init_grid(size,n_bomb):
    """
    crée une grille de jeu fonctionnelle
    :param size: taille de la grille souhaitée
    :param n_bomb: nombre de bombes dans la grille souhaitée
    :return: une matrice carrée de taille size, avec n_bomb -1 (bombes) et le reste d'entiers entre 0 et 8
    """
    grid = create_grid(size)
    size = len(grid)
    while n_bomb>0 and len(get_empty_tiles_positions(grid))>0:   # tant qu'il reste des bombes à placer
        (i,j) = get_new_position(grid)   # on trouve une position libre
        grid[i][j] = -1  # on place la bombe à cette position
        n_bomb -= 1
    for i in range(size):   # on parcourt la grille
        for j in range(size):
            if grid[i][j] != -1 :   # si la case n'est pas une bombe
                grid[i][j] = get_number_bombs(grid,i,j)   # on y met le nombre de bombes autour
    return grid


def init_grid_clic(size, n_bomb, i, j):
    """
    pour initialiser la grille juste après le clic et qu'on tombe sur un zéro au 1er clic
    :param size: taille de la grille
    :param n_bomb: nombre de bombes à placer dans la grille
    :param i: position du clic (ième ligne)
    :param j: position du clic (jème colonne)
    :return: une grille fonctionnelle avec un 0 en (i,j) (matrice carrée de taille size)
    """
    grid = init_grid(size,n_bomb)   # on crée une grille fonctionnelle quelconque
    while grid[i][j] != 0 :   # tant qu'il n'y a pas de 0 en (i,j) dans la grille
        grid = init_grid(size,n_bomb)   # on en crée une autre
    return grid


def get_all_tiles(grid):
    """
    (sert juste pour l'affichage sans tkinter, pour qu'on puisse voir la grille plus clairement sur la console)
    :param grid: la grille de jeu
    :return: la liste (de taille size*size) des valeurs de la grille
    """
    size = len(grid)
    tiles=[]
    for i in range(size):
        for j in range(size):
            tiles += [grid[i][j]]
    return tiles


def long_value(grid):
    """
    (sert juste pour l'affichage sans tkinter, pour que je puisse voir la grille plus clairement sur la console)
    :param grid: la grille de jeu
    :return: la taille de la plus longue chaîne de caractères dans la grille
    """
    L = get_all_tiles(grid)
    for i in range(len(L)):
        L[i] = len(str(L[i]))
    return max(L)


def grid_to_string(grid):
    """
    sert à afficher joliment la grille dans la console
    :param grid: la grille de jeu
    :return: une chaîne de caractères bien taillée pour afficher clairement la grille
    """
    size_case = long_value(grid)
    size = len(grid)
    result = size*(" "+(size_case*"="))
    result += "\n"
    for i in range(size):
        result += "|"
        for j in range(size):
            result += str(grid[i][j])+ (size_case-len(str(grid[i][j])))*" " +"|"
        result += "\n"
        result += size*(" "+(size_case*"="))
        result += "\n"
    return result


def zone_zeros(grid,i,j,deja_visit):
    """
    fonction à récurrence, renvoie toutes les cases à révéler lorsqu'on clique sur un zero en (i,j)
    :param grid: la grille de jeu
    :param i: le clic est à la ième ligne
    :param j: le clic est à la jème colonne
    :param deja_visit: paramètre nécessaire pour la récurrence, à initialiser à [] (correspond aux cases déjà vues par la fonction)
    :return: une liste de couples (i,j), positions des cases à révéler
    """
    size = len(grid)
    resultat = [(i,j)]
    if grid[i][j] == -1 :   # si c'est une bombe en (i,j)
        return[]
    if grid[i][j] > 0 :   # si c'est pas une bombe ni un 0
        return resultat   # la seule case à cliquer est en (i,j)
    if grid[i][j] == 0:   # si c'est un 0
        aux = [(i+1,j+1),(i,j+1),(i+1,j),(i-1,j-1),(i-1,j),(i,j-1),(i-1,j+1),(i+1,j-1)]   # liste des cases voisines
        for (ki,kj) in aux:   # on parcourt les cases voisines
            if (ki,kj) not in deja_visit and ki>=0 and kj>=0 and ki<size and kj<size:   # si on a pas encore visité la case et si elle existe
                deja_visit += [(ki,kj)]  # on signale qu'on l'a visitée
                resultat += zone_zeros(grid,ki,kj,deja_visit)   # on appelle la récurrence sur cette case
        return resultat
    return []


def cases_a_cliquer(grid,i,j,grid_bool):
    """
    renvoie toutes les cases à révéler si on clique sur n'importe quelle case (i,j)
    :param grid: la grille de jeu
    :param i: le clic est à la ième ligne
    :param j: le clic est à la jème colonne
    :param grid_bool: la liste de booléens qui sera nécessaire pour l'étape d'affichage
    cette grille est de même dimensions que la grille de jeu,
    elle est composée de couples de booléens
    le 1er booléen : il y a une bombe dans la case
    le 2e booléen : le joueur a placé un drapeau dans la case
    :return: une liste de couples (i',j')
    """
    if grid[i][j] != 0 :
        return [(i,j)]
    else :
        zone_zero = zone_zeros(grid,i,j,[])
        L = list(set(zone_zero))   # supprime les doublons
        for (i,j) in L:
            if grid_bool[i][j][0] or grid_bool[i][j][1]:  # s'il y a un drapeau en (i,j) ou si la case est déjà cliquée
                L.remove((i,j))
        return L

